const express = require('express');
const router = express.Router();
const members = require('../../Members');

router.get('/', (req, res) => {
    res.json(members);
});
router.get('/:id', (request, response) => {
    const found = members.some(member => member.id === parseInt(request.params.id));
    if (found) {
        response.json(members.filter(members => members.id === parseInt(request.params.id)));
    }
    else{
        response.status(400).json({msg: `No member with the id of ${request.params.id}`});
    }

});

module.exports = router;
